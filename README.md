# Locker Management System

## Overview
This project provides a simple locker management system implemented using JavaScript. It consists of functionality to add, view, store, lock and delete lockers.

## Files
- **main.js**: This file contains functions for managing lockers on the index.html page.
- **shared.js**: Contains the core classes `Locker` and `LockerList`, along with functions for handling local storage.
- **view.js**: Manages locker details and actions on the view.html page.

## main.js
### Functions
1. **displayLockers(data)**
   - Displays locker cards on the index.html page.
2. **addNewLocker()**
   - Handles the addition of new lockers.
3. **view(index)**
   - Redirects the user to the view.html page to view detailed locker information.
4. **displayDeleteButton(index)**
   - Displays the delete button for each locker card on the index.html page.
5. **deleteThisLocker(index)**
   - Deletes a locker based on the provided index.

## shared.js
### Functions
1. **checkIfDataExistsLocalStorage()**
   - Checks if locker data exists in local storage.
2. **updateLocalStorage(data)**
   - Updates locker data in local storage.
3. **getDataLocalStorage()**
   - Retrieves locker data from local storage.

## view.js
### Functions
1. **displayLockerInfo(locker)**
   - Displays detailed information about a locker on the view.html page.
2. **unlock(locker)**
   - Unlocks a locker.
3. **lockLocker()**
   - Locks a locker.
4. **closeLocker()**
   - Closes the locker details page.
5. **deleteThisLocker()**
   - Deletes a locker.

## Usage
- **index.html**: Use this page to view, add, and delete lockers.
- **view.html**: Use this page to view detailed information about a specific locker.

## Installation
Simply download the files and open index.html in a web browser to start managing your lockers.

## Developer Contact Details
*Name:* Anushka Reddy
*Contact:* anushkar614@gmail.com
